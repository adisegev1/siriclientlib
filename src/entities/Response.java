package entities;

/** This class used to supply detailed response info.
 * @author Adi
 *
 */
public class Response {

	/**
	 * General response message
	 */
	private String status;
	/**
	 * Response data, or reason in case of error.
	 */
	private String data;
	public String getstatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
