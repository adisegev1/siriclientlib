package entities;

import java.text.SimpleDateFormat;

import core.SiriClientMain;
import core.Utils;
import uk.org.siri.siri.MonitoredCallStructure;
import uk.org.siri.siri.MonitoredStopVisitStructure;
import uk.org.siri.siri.MonitoredVehicleJourneyStructure;
import zlib.ZDate;

/** This class represents single bus of specific lineRef.
 * @author Adi
 *
 */
public class SingleBusInLine {

	 /**
     * Aimed time bus exits it's origin station in HH:MM format
     */
    private String aimedDepartureTime;
    private boolean tripStarted;
    private int nextStation;
	private String expectedArrivalTime;
	private String direction;
	private String lineRef;
	public String getAimedDepartureTime() {
		return aimedDepartureTime;
	}
	public void setAimedDepartureTime(String aimedDepartureTime) {
		this.aimedDepartureTime = aimedDepartureTime;
	}
	public boolean isTripStarted() {
		return tripStarted;
	}
	public void setTripStarted(boolean tripStarted) {
		this.tripStarted = tripStarted;
	}
	public int getNextStation() {
		return nextStation;
	}
	public void setNextStation(int nextStation) {
		this.nextStation = nextStation;
	}
	public String getExpectedArrivalTime() {
		return expectedArrivalTime;
	}
	public void setExpectedArrivalTime(String timeNextBus) {
		this.expectedArrivalTime = timeNextBus;
	}
	 public SingleBusInLine(MonitoredStopVisitStructure msvs){
			
	        MonitoredVehicleJourneyStructure mvjs = msvs.getMonitoredVehicleJourney();
	        if (mvjs != null) {
	            if (mvjs.getDirectionRef() != null) {
	                this.direction = mvjs.getDirectionRef().getValue();
	            }
	            if (mvjs.getLineRef() != null) {
	                this.lineRef = mvjs.getLineRef().getValue();
	            }
	            if (mvjs.getOriginAimedDepartureTime() != null) {
	                this.aimedDepartureTime = Utils.formatDate(mvjs.getOriginAimedDepartureTime().toGregorianCalendar().getTime(),"HH:mm");
	            }
	            
//	            if(mvjs.getOperatorRef()!= null){
//	            opNum = Integer.valueOf(mvjs.getOperatorRef().getValue());
//	            }
//	            if (mvjs.getVehicleLocation() != null) {
//	                if (mvjs.getVehicleLocation().getLatitude() != null) {
//	                    lat = mvjs.getVehicleLocation().getLatitude().floatValue();
//	                }
//	                if (mvjs.getVehicleLocation().getLongitude() != null) {
//	                    lon = mvjs.getVehicleLocation().getLongitude().floatValue();
//	                }
//	            }
	           
	            MonitoredCallStructure mc = mvjs.getMonitoredCall();
	            if (mc != null) {
	            	   SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	                   SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");

	                   double timeNextd = Math.round(-ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime()).elapsed() / (float) ZDate.MINUTE);
	                   if (timeNextd <= 0) {
	                       setExpectedArrivalTime("0");
	                   } else if (timeNextd < 60) {
	                	   setExpectedArrivalTime("" + Math.round(-ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime()).elapsed() / (float) ZDate.MINUTE));
	                   } else if (d.format(ZDate.now()) == null ? d.format(ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime())) == null : d.format(ZDate.now()).equals(d.format(ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime())))) {
	                	   setExpectedArrivalTime("" + sdf.format(ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime())));
	                   } else {
	                	   setExpectedArrivalTime(Utils.formatDate(ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime()),"dd/MM/yy-HH:mm"));
	                   }
	                   
		                if (mc.getAimedArrivalTime() != null) {
		                    tripStarted = false;
		                } else {
		                	tripStarted = true;
		                }
	            if(mc.getStopPointRef() != null){
	            	nextStation = Integer.valueOf(mc.getStopPointRef().getValue());
	            }
	             
	          
	            }
	            
	            if(Utils.debug)
	            SiriClientMain.getInstance().getLogger().logEvent("MonitoredVehicleJourneyStructure set");    
	        }else {
	            if(Utils.debug)
	         SiriClientMain.getInstance().getLogger().logEvent("MonitoredVehicleJourneyStructure null");
	        }
	 }
	public String getLineRef() {
		return lineRef;
	}
	public void setLineRef(String lineRef) {
		this.lineRef = lineRef;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}

}
