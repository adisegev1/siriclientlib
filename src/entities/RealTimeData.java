/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import core.SiriClientMain;
import core.Utils;
import uk.org.siri.siri.MonitoredCallStructure;
import uk.org.siri.siri.MonitoredStopVisitStructure;
import uk.org.siri.siri.MonitoredVehicleJourneyStructure;
import zlib.ZDate;

/**
 * This class represent basic Real Time data about specific stop.
 * @author Adi
 */
public class RealTimeData {

    String numberBus;
    String timeNextBus;
    String direction;
    /**
     * Aimed time bus exits it's origin station in HH:MM format
     */
    String aimedDepartureTime;
	boolean tripStarted;
    float lat;
    float lon;
    int nextStation;
    Integer opNum;
    Integer lastStatus;
    String currentStation;
	private String lineRef;
	private String tripId; 

    public String getAimedDepartureTime() {
		return aimedDepartureTime;
	}

	public void setAimedDepartureTime(String aimedDepartureTime) {
		this.aimedDepartureTime = aimedDepartureTime;
	}

	public boolean isTripStarted() {
		return tripStarted;
	}

	public void setTripStarted(boolean tripStarted) {
		this.tripStarted = tripStarted;
	}
    public String getCurrentStation() {
		return currentStation;
	}

	public void setCurrentStation(String currentStation) {
		this.currentStation = currentStation;
	}

	public Integer getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(Integer lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public RealTimeData() {
    }

    public RealTimeData(MonitoredStopVisitStructure msvs) {
    	
        MonitoredVehicleJourneyStructure mvjs = msvs.getMonitoredVehicleJourney();
        if (mvjs != null) {
            if (mvjs.getDirectionRef() != null) {
                this.direction = mvjs.getDirectionRef().getValue();
            }
            if (mvjs.getPublishedLineName() != null) {
                this.numberBus = mvjs.getPublishedLineName().getValue();
            }
            if (mvjs.getLineRef() != null) {
                this.lineRef = mvjs.getLineRef().getValue();
            }
            if (mvjs.getOriginAimedDepartureTime() != null) {
                this.aimedDepartureTime = Utils.formatDate(mvjs.getOriginAimedDepartureTime().toGregorianCalendar().getTime(),"HH:mm");
            }
            
            if(mvjs.getOperatorRef()!= null){
            opNum = Integer.valueOf(mvjs.getOperatorRef().getValue());
            }
            if (mvjs.getVehicleLocation() != null) {
                if (mvjs.getVehicleLocation().getLatitude() != null) {
                    lat = mvjs.getVehicleLocation().getLatitude().floatValue();
                }
                if (mvjs.getVehicleLocation().getLongitude() != null) {
                    lon = mvjs.getVehicleLocation().getLongitude().floatValue();
                }
            }
           
            MonitoredCallStructure mc = mvjs.getMonitoredCall();
            if (mc != null) {
                timeNextBus = String.valueOf(ZDate.fromXMLGregorianCalendar(mc.getExpectedArrivalTime()).getTime());
                
                if (mc.getAimedArrivalTime() != null) {
                    tripStarted = false;
                } else {
                	tripStarted = true;
                }
          
            }
            
            if (mvjs.getFramedVehicleJourneyRef() != null && mvjs.getFramedVehicleJourneyRef().getDatedVehicleJourneyRef() != null) {
                this.tripId = mvjs.getFramedVehicleJourneyRef().getDatedVehicleJourneyRef();
            }

            if(Utils.debug)
            SiriClientMain.getInstance().getLogger().logEvent("MonitoredVehicleJourneyStructure set: "+timeNextBus+", "+numberBus);    
        }else {
            if(Utils.debug)
         SiriClientMain.getInstance().getLogger().logEvent("MonitoredVehicleJourneyStructure null");
        }

    }

public String getLineRef() {
		return lineRef;
	}

	public void setLineRef(String lineRef) {
		this.lineRef = lineRef;
	}

	//    public RealTimeData(String numberBus, String timeNextBus, String direction, float lat, float lon) {
//        this.numberBus = numberBus;
//        this.timeNextBus = timeNextBus;
//        this.direction = direction;
//        this.lat = lat;
//        this.lon = lon;
//    }
    public RealTimeData(String numberBus, String timeNextBus, String direction, float lat, float lon, Integer station, Integer opNum, Integer LastStatus) {
        this.numberBus = numberBus;
        this.timeNextBus = timeNextBus;
        this.direction = direction;
        this.lat = lat;
        this.lon = lon;
        this.nextStation = station;
        this.opNum = opNum;
        this.lastStatus = LastStatus;
    }

    public RealTimeData(String numberBus, String timeNextBus, String direction, Integer station, Integer opNum, Integer LastStatus) {
        this.numberBus = numberBus;
        this.timeNextBus = timeNextBus;
        this.direction = direction;
        this.nextStation = station;
        this.opNum = opNum;
        this.lastStatus = LastStatus;
    }

    public String getNumberBus() {
        return numberBus;
    }

    public void setNumberBus(String numberBus) {
        this.numberBus = numberBus;
    }

    public String getTimeNextBus() {
        return timeNextBus;
    }

    public void setTimeNextBus(String timeNextBus) {
        this.timeNextBus = timeNextBus;
    }

//    public void setTimeNextBus(int timeNextBus) {
//        this.timeNextBus = ""+timeNextBus+"";
//    }
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public int getNextStation() {
        return nextStation;
    }

    public void setNextStation(int station) {
        this.nextStation = station;
    }

    public Integer getOpNum() {
        return opNum;
    }

    public void setOpNum(Integer opNum) {
        this.opNum = opNum;
    }

}
