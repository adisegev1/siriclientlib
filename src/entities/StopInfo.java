package entities;

import java.util.ArrayList;
import java.util.Date;

import uk.org.siri.siri.LocationStructure;
import uk.org.siri.siri.MonitoredCallStructure;
import uk.org.siri.siri.MonitoredStopVisitStructure;
import uk.org.siri.siri.MonitoredVehicleJourneyStructure;

public class StopInfo {
	 private String monitoringRef, line, direction, lineName, operatorId, destination;
     private ArrayList<Date> etaList;
     private Date recordedAtTime, aimedDepartureTime, aimedArrivalTime;
     private LocationStructure location;
     private boolean vehicleAtStop;
     private String arrivalStatus;
     private String confidenceLevel;

     public StopInfo(MonitoredStopVisitStructure msvs) {
         if (msvs != null) {
             if (msvs.getMonitoringRef() != null) {
                 this.monitoringRef = msvs.getMonitoringRef().getValue();
             }
             if (msvs.getRecordedAtTime() != null) {
                 this.recordedAtTime = msvs.getRecordedAtTime().toGregorianCalendar().getTime();
             }
             MonitoredVehicleJourneyStructure mvjs = msvs.getMonitoredVehicleJourney();
             if (mvjs != null) {
                 if (mvjs.getLineRef() != null) {
                     this.line = mvjs.getLineRef().getValue();
                 }
                 if (mvjs.getDirectionRef() != null) {
                     this.direction = mvjs.getDirectionRef().getValue();
                 }
                 if (mvjs.getPublishedLineName() != null) {
                     this.lineName = mvjs.getPublishedLineName().getValue();
                 }
                 if (mvjs.getOperatorRef() != null) {
                     this.operatorId = mvjs.getOperatorRef().getValue();
                 }
                 if (mvjs.getDestinationRef() != null) {
                     this.destination = mvjs.getDestinationRef().getValue();
                 }
                 if (mvjs.getVehicleLocation() != null) {
                     this.location = mvjs.getVehicleLocation();
                 }
                 if (mvjs.getOriginAimedDepartureTime() != null) {
                     this.aimedDepartureTime = mvjs.getOriginAimedDepartureTime().toGregorianCalendar().getTime();
                 }
                 if (mvjs.getConfidenceLevel() != null) {
                     this.confidenceLevel = mvjs.getConfidenceLevel().value();
                 }
                 MonitoredCallStructure mc = mvjs.getMonitoredCall();
                 if (mc != null) {
                     if (mc.getStopPointRef() != null) {
                         this.monitoringRef = mc.getStopPointRef().getValue();
                     }
                     if (mc.getExpectedArrivalTime() != null) {
                         etaList = new ArrayList<>();
                         this.etaList.add(mc.getExpectedArrivalTime().toGregorianCalendar().getTime());
                     }
                     if (mc.getAimedDepartureTime() != null) {
                         this.aimedDepartureTime = mc.getAimedDepartureTime().toGregorianCalendar().getTime();
                     }
                     if (mc.getAimedArrivalTime() != null) {
                         this.aimedArrivalTime = mc.getAimedArrivalTime().toGregorianCalendar().getTime();
                     }
                     if (mc.isVehicleAtStop() != null) {
                         this.vehicleAtStop = mc.isVehicleAtStop();
                         
                     }
                     if (mc.getArrivalStatus() != null) {
                         this.arrivalStatus = mc.getArrivalStatus().value();
                     }
                 }
             }
         }
     }

     /**
      * @return the monitoringRef
      */
     public String getMonitoringRef() {
         return monitoringRef;
     }

     /**
      * @return the line
      */
     public String getLine() {
         return line;
     }

     /**
      * @return the direction
      */
     public String getDirection() {
         return direction;
     }

     /**
      * @return the lineName
      */
     public String getLineName() {
         return lineName;
     }

     /**
      * @return the operatorId
      */
     public String getOperatorId() {
         return operatorId;
     }

     /**
      * @return the destination
      */
     public String getDestination() {
         return destination;
     }

     /**
      * @return the etaList
      */
     public ArrayList<Date> getEtaList() {
         return etaList;
     }

     /**
      * @return the recordedAtTime
      */
     public Date getRecordedAtTime() {
         return recordedAtTime;
     }

     /**
      * @return the aimedDepartureTime
      */
     public Date getAimedDepartureTime() {
         return aimedDepartureTime;
     }

     /**
      * @return the aimedArrivalTime
      */
     public Date getAimedArrivalTime() {
         return aimedArrivalTime;
     }

     /**
      * @return the location
      */
     public LocationStructure getLocation() {
         return location;
     }

     /**
      * @return the vehicleAtStop
      */
     public boolean isVehicleAtStop() {
         return vehicleAtStop;
     }

     private String getArrivalStatus() {
         return arrivalStatus;
     }

     public String getConfidenceLevel() {
         return confidenceLevel;
     }
 }

