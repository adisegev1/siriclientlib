/*
 * This Class gets data from the Siri Server.
 */
package core;

import static core.SiriClientMain.SM_VERSION;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import uk.org.siri.siri.LineRefStructure;
import uk.org.siri.siri.MessageQualifierStructure;
import uk.org.siri.siri.MonitoringRefStructure;
import uk.org.siri.siri.ParticipantRefStructure;
import uk.org.siri.siri.ServiceDeliveryStructure;
import uk.org.siri.siri.ServiceRequestStructure;
import uk.org.siri.siri.StopMonitoringDetailEnumeration;
import uk.org.siri.siri.StopMonitoringRequestStructure;
import zlib.Settings;
import zlib.ZDate;

/**
 *
 * @author Adi
 */
class StopMonitoringQueriesOldProtocol extends StopMonitoringQueries{

    SiriClientMain siriClientMain = SiriClientMain.getInstance();
    private final SiriServices siriServices;
    private final SOAPPort siriWSPort;
    private final Settings settings;
    private Duration previewInterval;
    private String requestorRef;
    private long messageIdentifier;
    private final Set<String> finished = new HashSet<>(200);//<signNum_RouteNum> lines finished for day until predicted time should be sent (i.e. 30 before depart)--- ver 1.00.47 made static to support multiple SiriServers
    final static int VISUAL_ORDER = 0;
    final static int LOGICAL_ORDER = 1;
    private final String version;

    StopMonitoringQueriesOldProtocol(Settings appSettings) {

        settings = appSettings;
        setPreviewInterval();
        requestorRef = settings.getProperty("requestor ref");
        if (requestorRef == null) {
            requestorRef = "ISRDEV01";
        }
        messageIdentifier = 1;
        siriServices = SiriServices.getInstance(settings);
        siriWSPort = siriServices.getSiriPort();
        version = settings.getProperty(SM_VERSION)== null? "IL2.7" :settings.getProperty(SM_VERSION);
      
        //setWsTimeOut();
    }
    public Set<String> getFinished() {
        return finished;
    }

    MessageQualifierStructure getMessageIdentifier() {
        if (messageIdentifier == Long.MAX_VALUE) {
            messageIdentifier = 1;
        }
        MessageQualifierStructure mqs = new MessageQualifierStructure();
        mqs.setValue(Long.toString(messageIdentifier++));
        return (mqs);
    }

    private void setWsTimeOut() {
    	int reqTimeout = 20000;
    	String requestTimeout = settings.getProperty("requestTimeout")== null? "20000" :settings.getProperty("requestTimeout");
    	if(Utils.isInteger(requestTimeout)){
    		reqTimeout = Integer.parseInt(requestTimeout);
    	}
    	int conTimeout = 20000;
    	String connectTimeout = settings.getProperty("connectTimeout")== null? "20000" :settings.getProperty("connectTimeout");
    	if(Utils.isInteger(connectTimeout)){
    		conTimeout = Integer.parseInt(connectTimeout);
    	}
    	
        Map<String, Object> requestContext = ((BindingProvider) siriWSPort).getRequestContext();
        requestContext.put("com.sun.xml.ws.request.timeout", reqTimeout);
        requestContext.put("com.sun.xml.ws.connect.timeout", conTimeout);
        
    }


    private void fillGeneralRequestData(ServiceRequestStructure srs, XMLGregorianCalendar now) {
        srs.setRequestTimestamp(now);
        srs.setMessageIdentifier(getMessageIdentifier());

        ParticipantRefStructure prs = new ParticipantRefStructure();
        prs.setValue(requestorRef);
        srs.setRequestorRef(prs);
    }

    private void fillIndividualRequestData(ServiceRequestStructure srs, StopMonitoringRequestParams requestParams,
            XMLGregorianCalendar now, String version) {
        StopMonitoringRequestStructure smrs = new StopMonitoringRequestStructure();

        MonitoringRefStructure reqMrs = requestParams.getMonitoringRef();
        LineRefStructure reqLrs = requestParams.getLineRef();

        if (reqMrs == null) {
            reqMrs = new MonitoringRefStructure();
            reqMrs.setValue("ALL");
        }
        smrs.setMonitoringRef(reqMrs);

        if (reqLrs != null && !reqLrs.getValue().isEmpty()) {
            smrs.setLineRef(reqLrs);
        } else {
            reqLrs = new LineRefStructure();
            reqLrs.setValue("");
        }

        if (requestParams.getPreviewInterval() != null) {
            smrs.setPreviewInterval(requestParams.getPreviewInterval());
//        } else {
//            smrs.setPreviewInterval(getPreviewInterval());
        }
        if (requestParams.getStartTime() != null) {
            smrs.setStartTime(requestParams.getStartTime());
        }
        if (requestParams.getMaxStopVisit() != null) {
            smrs.setMaximumStopVisits(requestParams.getMaxStopVisit());
        }
        if (requestParams.getMinStopVisitPerLine() != null) {
            smrs.setMinimumStopVisitsPerLine(requestParams.getMinStopVisitPerLine());
        }
        if (version == null) {
            version = settings.getProperty(SM_VERSION);
        }
        smrs.setVersion(version == null ? "IL2.7" : version);
        smrs.setRequestTimestamp(now);
        smrs.setMessageIdentifier(getMessageIdentifier());

        if ("true".equals(settings.getProperty("Full Detail"))) {
            smrs.setStopMonitoringDetailLevel(StopMonitoringDetailEnumeration.FULL);
        }
        srs.getStopMonitoringRequest().add(smrs);
    }

    @Override
    public Object GetStopArrivalTimes(StopMonitoringRequestParams requestParams) {
        try {
            XMLGregorianCalendar now = Utils.now();
            ServiceRequestStructure srs = new ServiceRequestStructure();
            fillGeneralRequestData(srs, now);
            fillIndividualRequestData(srs, requestParams, now, version); // initialize srs with requestParams.
           
            return siriWSPort.getStopMonitoringService(srs);
        } catch (Exception e) {
            siriClientMain.getLogger().logException(e);
            return e;
        }
    }

    @Override
    public Object GetStopArrivalTimesMultipleRequests(List<StopMonitoringRequestParams> requestParamsList) {
        XMLGregorianCalendar now = Utils.now();
        ServiceRequestStructure srs = new ServiceRequestStructure();
        fillGeneralRequestData(srs, now);
        for (StopMonitoringRequestParams requestParams : requestParamsList) {
            fillIndividualRequestData(srs, requestParams, now, version); // initialize srs with requestParams.
        }
       
        return siriWSPort.getStopMonitoringService(srs);
    }

    /**
     * @return the previewInterval
     */
    public Duration getPreviewInterval() {
        return previewInterval;
    }

 
    public void setPreviewInterval() {
    	
        if ("true".equals(settings.getProperty("24hr Preview"))) {
            try {
                previewInterval = DatatypeFactory.newInstance().newDuration(24 * 60 * 60 * 1000);//24 hr duration
            } catch (Exception e) {
                siriClientMain.getLogger().logException(e);
            }
        }
    }

}
