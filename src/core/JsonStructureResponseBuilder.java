/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import core.Constants.ErrorReasons;
import core.StopMonitoringRequestParams;
import entities.RealTimeData;
import entities.Response;
import entities.SingleBusInLine;
import entities.StopInfo;
import uk.org.siri.siri.MonitoredStopVisitStructure;
import uk.org.siri.siri.ServiceDeliveryErrorConditionStructure;
import uk.org.siri.siri.ServiceDeliveryStructure;
import uk.org.siri.siri.ServiceDeliveryStructure.ErrorCondition;
import uk.org.siri.siri.StopMonitoringDeliveryStructure;
import zlib.Zson;

/**
 *
 * @author Adi
 */
public class JsonStructureResponseBuilder {

    private final StopMonitoringQueries stopMonitoringQueries;
    private final Object object = new Object();

    JsonStructureResponseBuilder(StopMonitoringQueries stopMonitoringQueries) {
        this.stopMonitoringQueries = stopMonitoringQueries;
    }
    String getBasicRealData(ArrayList<String> monitoringRefStr, String lineRefStr, String startTimeStr, String maxStopVisitsStr,
            String minStopVisitsPerLineStr, String previewIntervalStr, boolean returnMultipleTripsPerLIne,
            boolean returnBusesPassedStation, boolean returnBusesCurrentStation) {
//		synchronized (object) {

        if (lineRefStr == null) {
            lineRefStr = "all";
        }
        if (startTimeStr == null) {
            startTimeStr = "";
        }
        if (maxStopVisitsStr == null) {
            maxStopVisitsStr = "";
        }
        if (minStopVisitsPerLineStr == null) {
            minStopVisitsPerLineStr = "";
        }
        if (previewIntervalStr == null) {
            previewIntervalStr = String.valueOf(1440);
        }

		
        StopMonitoringRequestParams smrp;
        ArrayList<StopMonitoringRequestParams> smrpList = new ArrayList<>();

		
        for (String stationId : monitoringRefStr) {
            if (stationId == null) {
                stationId = "all";
            }
            smrp = new StopMonitoringRequestParams(stationId, lineRefStr, startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, previewIntervalStr);
            smrpList.add(smrp);
        }
        long start = System.currentTimeMillis();
        if (Utils.testSiriResponses) {
            System.out.println("Thread Id: " + Thread.currentThread().getId() + " 1 before GetStopArrivalTimesMultipleRequests: " + start);
        }

        Object sds;
        try {
            sds = stopMonitoringQueries.GetStopArrivalTimesMultipleRequests(smrpList);

        } catch (Exception ex) {
            ex.printStackTrace();
            if (Utils.testSiriResponses) {
                System.out.println("Thread Id: " + Thread.currentThread().getId() + " failed GetStopArrivalTimesMultipleRequests: " + ex.getMessage());
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logException(ex);
            }
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ex.getMessage());
            return Zson.toJson(response);
        }

        if (Utils.debug) {
            SiriClientMain.getInstance().getLogger()
                    .logEvent("after GetStopArrivalTimesMultipleRequests: " + (System.currentTimeMillis() - start));
        }

        if (sds instanceof ServiceDeliveryStructure) {
            if (Utils.testSiriResponses) {
                System.out.println("Thread Id: " + Thread.currentThread().getId() + " 2 after GetStopArrivalTimesMultipleRequests: " + (System.currentTimeMillis() - start) + (((ServiceDeliveryStructure) sds).getStopMonitoringDelivery() != null ? ". Num of stops: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size() : " null"));
            }

            ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
            if (errorCondition != null) {
                if (errorCondition.getDescription() != null) {
                    if (errorCondition.getDescription().getValue() != null) {
                        if (Utils.testSiriResponses) {
                            System.out.println("Thread Id: " + Thread.currentThread().getId() + " 2 after GetStopArrivalTimesMultipleRequests ErrorCondition: " + errorCondition.getDescription().getValue());
                        }

                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 120");
                        }
                        Response response = new Response();
                        response.setStatus(Constants.ResponseStatuses.FAILED);
                        response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                        return Zson.toJson(response);
                    }
                }
            }

            return extractBasicRealData((ServiceDeliveryStructure) sds, returnMultipleTripsPerLIne,
                    returnBusesPassedStation, returnBusesCurrentStation);
        } else {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 138");
            }
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
            return Zson.toJson(response);
        }
//		}
    }

    /**
     * This method returns basic real time data about lines or stops.
     *
     * @param stationId
     * @param lineId
     * @param startTimeStr
     * @param maxStopVisitsStr
     * @param minStopVisitsPerLineStr
     * @param maxExcpectedArrivalTime in minutes
     * @param returnMultipleTripsPerLIne if true returns multiple buses with
     * same lineRef, else, returns only the next bus per lineRef
     * @param returnBusesPassedStation if true returns also buses arrived and
     * passed given station, else, returns only the buses suppose to pass
     * through given station and didn't arrived yet.
     * @return If we've successfully got data, RealTimeData list structured as
     * JSONArray. See {@link RealTimeData}.<br>
     * In case of error, ErrorResponse structured as JSONObject. See
     * {@link Response}
     *
     */
    String getBasicRealData(String monitoringRefStr, String lineRefStr, String startTimeStr, String maxStopVisitsStr,
            String minStopVisitsPerLineStr, String previewIntervalStr, boolean returnMultipleTripsPerLIne,
            boolean returnBusesPassedStation, boolean returnBusesCurrentStation) {
//		synchronized (object) {

        if (monitoringRefStr == null) {
            monitoringRefStr = "all";
        }
        if (lineRefStr == null) {
            lineRefStr = "all";
        }
        if (startTimeStr == null) {
            startTimeStr = "";
        }
        if (maxStopVisitsStr == null) {
            maxStopVisitsStr = "";
        }
        if (minStopVisitsPerLineStr == null) {
            minStopVisitsPerLineStr = "";
        }
        if (previewIntervalStr == null) {
            previewIntervalStr = String.valueOf(60);
        }
        StopMonitoringRequestParams smrp = new StopMonitoringRequestParams(monitoringRefStr,
                lineRefStr, startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, previewIntervalStr);
        long start = System.currentTimeMillis();
        if (Utils.testSiriResponses) {
            System.out.println("Thread Id: " + Thread.currentThread().getId() + " 1 before GetStopArrivalTimes: " + new Date());
        }

        Object sds;
        try {
            if(Utils.debug){
                SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringRequestParams: "+smrp.getPreviewInterval().getMinutes()+" previewIntervalStr: "+previewIntervalStr);
            }
            sds = stopMonitoringQueries.GetStopArrivalTimes(smrp);

        } catch (Exception ex) {
            ex.printStackTrace();
            if (Utils.testSiriResponses) {
                System.out.println("Thread Id: " + Thread.currentThread().getId() + " failed GetStopArrivalTimes: " + ex.getMessage());
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logException(ex);
            }
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ex.getMessage());
            return Zson.toJson(response);
        }

        if (Utils.debug) {
            SiriClientMain.getInstance().getLogger()
                    .logEvent("after GetStopArrivalTimes: " + (System.currentTimeMillis() - start));
        }

        if (sds instanceof ServiceDeliveryStructure) {
            if (Utils.testSiriResponses) {
                System.out.println("Thread Id: " + Thread.currentThread().getId() + " 2 after GetStopArrivalTimes: " + (System.currentTimeMillis() - start) + (((ServiceDeliveryStructure) sds).getStopMonitoringDelivery() != null ? ". Num of stops: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size() : " null"));
            }

            ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
            if (errorCondition != null) {
                if (errorCondition.getDescription() != null) {
                    if (errorCondition.getDescription().getValue() != null) {
                        if (Utils.testSiriResponses) {
                            System.out.println("Thread Id: " + Thread.currentThread().getId() + " 2 after GetStopArrivalTimes ErrorCondition: " + errorCondition.getDescription().getValue());
                        }
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 235");
                        }
                        Response response = new Response();
                        response.setStatus(Constants.ResponseStatuses.FAILED);
                        response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                        return Zson.toJson(response);
                    }
                }
            }

            return extractBasicRealData((ServiceDeliveryStructure) sds, returnMultipleTripsPerLIne,
                    returnBusesPassedStation, returnBusesCurrentStation);
        } else {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 249");
            }
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
            return Zson.toJson(response);
        }
//		}
    }

    private String extractBasicRealData(ServiceDeliveryStructure sds, boolean returnMultipleBusesPerLIne,
            boolean returnBusesPassedStation, boolean returnBusesCurrentStation) {
        long startExtractRealData = System.currentTimeMillis();
        // represent list of real time data for lines entering to specific
        // station.
        ArrayList<RealTimeData> realTimeDataList = new ArrayList<>();

        // holds the lineRefs of the lines entering specific station
        ArrayList<String> lineRefsList = new ArrayList<>();

        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }

            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }
                    ServiceDeliveryErrorConditionStructure errorCondition = smds.getErrorCondition();
                    if (errorCondition != null) {
                        if (errorCondition.getDescription() != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 290");
                            }
                            if (errorCondition.getDescription().getValue() != null) {
                                Response response = new Response();
                                response.setStatus(Constants.ResponseStatuses.FAILED);
                                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                                return Zson.toJson(response);
                            }
                        }
                    }

                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            RealTimeData realTimeData = new RealTimeData(msvs);

                            if (!lineRefsList.contains(realTimeData.getLineRef())) {
                                lineRefsList.add(realTimeData.getLineRef());
                                realTimeDataList.add(realTimeData);
                            } else {
                                if (returnMultipleBusesPerLIne) {
                                    realTimeDataList.add(realTimeData);
                                }
                            }

                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }

        if (!returnBusesCurrentStation) {
            long start = System.currentTimeMillis();
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(realTimeDataList, new TypeToken<ArrayList<RealTimeData>>() {
            }.getType());

            if (!element.isJsonArray()) {
                // fail appropriately
                SiriClientMain.getInstance().getLogger().logEvent("failed to convert to array " + element.toString());
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData(ErrorReasons.LIST_PARSE_ERROR);
                return Zson.toJson(response);
            }

            JsonArray jsonArray = element.getAsJsonArray();
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.SUCCESS);
            response.setData(jsonArray.toString());

            String finalResponse = Zson.toJson(response);

            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logEvent("after final response: " + (System.currentTimeMillis() - start));
            }

            return finalResponse;
        }
        if (realTimeDataList.size() > 0) {

            addNextStationDetails(lineRefsList, realTimeDataList, returnBusesPassedStation);

        }
        long start = System.currentTimeMillis();
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(realTimeDataList, new TypeToken<ArrayList<RealTimeData>>() {
        }.getType());

        if (!element.isJsonArray()) {
            // fail appropriately
            SiriClientMain.getInstance().getLogger().logEvent("failed to convert to array " + element.toString());
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(ErrorReasons.LIST_PARSE_ERROR);
            return Zson.toJson(response);
        }

        JsonArray jsonArray = element.getAsJsonArray();
        Response response = new Response();
        response.setStatus(Constants.ResponseStatuses.SUCCESS);
        response.setData(jsonArray.toString());

        String finalResponse = Zson.toJson(response);

        if (Utils.debug) {
            SiriClientMain.getInstance().getLogger()
                    .logEvent("after final response: " + (System.currentTimeMillis() - start));
        }
        if (Utils.testSiriResponses) {
            System.out.println("Thread Id: " + Thread.currentThread().getId() + " 4 finish extract: " + (System.currentTimeMillis() - startExtractRealData));
        }
        return finalResponse;
    }

    public Object getFullLinesDetails(ArrayList<String> lineRefs, boolean returnMultipleBusesPerLIne) {
        if (lineRefs == null) {
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(ErrorReasons.MISSING_FIELD_LINEREF);
            return response;
        }
        StopMonitoringRequestParams smrp;
        ArrayList<StopMonitoringRequestParams> smrpList = new ArrayList<>();

        if (!returnMultipleBusesPerLIne) {
            for (String lineRef : lineRefs) {
                smrp = new StopMonitoringRequestParams("all", lineRef, "", "", "", "1440");
                smrpList.add(smrp);
            }
            long start = System.currentTimeMillis();
            Object sds;
            try {
                sds = stopMonitoringQueries.GetStopArrivalTimesMultipleRequests(smrpList);
            } catch (Exception ex) {
                ex.printStackTrace();
                if (Utils.testSiriResponses) {
                    System.out.println("Thread Id: " + Thread.currentThread().getId() + " 3 failed GetStopArrivalTimesMultipleRequests: " + ex.getMessage());
                }
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logException(ex);
                }
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ex.getMessage());
                return response;
            }

            if (Utils.testSiriResponses) {
                System.out.println("Thread Id: " + Thread.currentThread().getId() + " 3 after GetStopArrivalTimesMultipleRequests : " + (System.currentTimeMillis() - start) + ". Num of trips: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size());
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logEvent("after GetStopArrivalTimesMultipleRequests " + (System.currentTimeMillis() - start));
            }
            if (sds instanceof ServiceDeliveryStructure) {
                ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
                if (errorCondition != null) {
                    if (errorCondition.getDescription() != null) {
                        if (errorCondition.getDescription().getValue() != null) {
                            if (Utils.testSiriResponses) {
                                System.out.println("Thread Id: " + Thread.currentThread().getId() + " 2 after GetStopArrivalTimesMultipleRequests ErrorCondition: " + errorCondition.getDescription().getValue());
                            }
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 449");
                            }
                            Response response = new Response();
                            response.setStatus(Constants.ResponseStatuses.FAILED);
                            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                            return Zson.toJson(response);
                        }
                    }
                }
                return extractNewSingleBusInLineData((ServiceDeliveryStructure) sds);
            } else {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 461");
                }
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
                return response;
            }
        } else {
            for (String lineRef : lineRefs) {
                smrp = new StopMonitoringRequestParams("all", lineRef, "", "", "", "1440");
                smrpList.add(smrp);
            }
            long start = System.currentTimeMillis();

            Object sds = stopMonitoringQueries.GetStopArrivalTimesMultipleRequests(smrpList);
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logEvent("after GetStopArrivalTimesMultipleRequests " + (System.currentTimeMillis() - start));
            }
            if (sds instanceof ServiceDeliveryStructure) {
                ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
                if (errorCondition != null) {
                    if (errorCondition.getDescription() != null) {
                        if (errorCondition.getDescription().getValue() != null) {
                            if (Utils.testSiriResponses) {
                                System.out.println("Thread Id: " + Thread.currentThread().getId() + " after GetStopArrivalTimesMultipleRequests ErrorCondition: " + errorCondition.getDescription().getValue());
                            }
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("SERVER_ERROR line 489");
                            }
                            Response response = new Response();
                            response.setStatus(Constants.ResponseStatuses.FAILED);
                            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                            return Zson.toJson(response);
                        }
                    }
                }
                return extractNewMultipleBusInLineData((ServiceDeliveryStructure) sds);
            } else {
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData((Constants.ErrorReasons.SERVER_ERROR + ": " + (Exception) sds).toString());
                return response.toString();
            }
        }

    }

    private boolean isSameBusPassedStation(RealTimeData realTimeData, SingleBusInLine singleBusesList) {
        if (!realTimeData.getLineRef().equalsIgnoreCase(singleBusesList.getLineRef())) {
            return false;
        }
        if (!realTimeData.getDirection().equalsIgnoreCase(singleBusesList.getDirection())) {
            return false;
        }
        return true;
    }

    private boolean isSameBus(RealTimeData realTimeData, SingleBusInLine singleBusesList) {
        if (!realTimeData.getLineRef().equalsIgnoreCase(singleBusesList.getLineRef())) {
            return false;
        }
        if (!realTimeData.getAimedDepartureTime().equalsIgnoreCase(singleBusesList.getAimedDepartureTime())) {
            return false;
        }
        if (!realTimeData.getDirection().equalsIgnoreCase(singleBusesList.getDirection())) {
            return false;
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    private void addNextStationDetails(ArrayList<String> lineRefs, ArrayList<RealTimeData> realTimeDataList,
            boolean returnBusesPassedStation) {
        try {
            // represent full trips list for all linerefs.
            Object obj = getFullLinesDetails(lineRefs, false);

            if (!(obj instanceof HashMap<?, ?>)) {
                return;
            }

            HashMap<String, ArrayList<SingleBusInLine>> fullTripsList = (HashMap<String, ArrayList<SingleBusInLine>>) obj;

            for (int j = 0; j < realTimeDataList.size(); j++) {
                ArrayList<SingleBusInLine> singleBusesList = fullTripsList.get(realTimeDataList.get(j).getLineRef());
                if (singleBusesList == null) {
                    continue;
                }
                for (SingleBusInLine trip : singleBusesList) {
                    if (realTimeDataList.get(j).getNextStation() == 0 && isSameBus(realTimeDataList.get(j), trip)) {
                        realTimeDataList.get(j).setNextStation(trip.getNextStation());
                    }
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * This method returns details about line's next bus station.<br>
     * Each item structured as {@link SingleBusInLine} object.
     *
     * @param lineRef lineId
     * @param returnMultipleBusesPerLIne
     * @return if returnMultipleBuses is true, returns all buses for the given
     * lineRef, as list of {@link SingleBusInLine} objects structured as
     * {@link JSONArray}.<br>
     * Else returns single {@link SingleBusInLine} objects as
     * {@link JSONObject}.<br>
     */
    public Object getLineNextStationDetails(String lineRef, boolean returnMultipleBusesPerLIne) {
        if (lineRef == null) {
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(ErrorReasons.MISSING_FIELD_LINEREF);
            return response;
        }
        StopMonitoringRequestParams smrp;
        if (!returnMultipleBusesPerLIne) {

            smrp = new StopMonitoringRequestParams("all", lineRef, "", "1", "", "1440");
            long start = System.currentTimeMillis();
            Object sds = stopMonitoringQueries.GetStopArrivalTimes(smrp);
            if (Utils.testSiriResponses) {
                System.out.println("after GetStopArrivalTimes: " + (System.currentTimeMillis() - start) + ". Size: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size());
            }
            if (sds instanceof ServiceDeliveryStructure) {
                return extractSingleBusInLineData((ServiceDeliveryStructure) sds);
            } else {
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
                return response;
            }
        } else {
            smrp = new StopMonitoringRequestParams("all", lineRef, "", "", "", "1440");
            long start = System.currentTimeMillis();
            Object sds = stopMonitoringQueries.GetStopArrivalTimes(smrp);
            if (Utils.testSiriResponses) {
                System.out.println("after GetStopArrivalTimes: " + (System.currentTimeMillis() - start) + ". Size: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size());
            }
            if (sds instanceof ServiceDeliveryStructure) {
                ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
                if (errorCondition != null) {
                    if (errorCondition.getDescription() != null) {
                        if (errorCondition.getDescription().getValue() != null) {
                            if (Utils.testSiriResponses) {
                                System.out.println("Thread Id: " + Thread.currentThread().getId() + " after GetStopArrivalTimes ErrorCondition: " + errorCondition.getDescription().getValue());
                            }

                            Response response = new Response();
                            response.setStatus(Constants.ResponseStatuses.FAILED);
                            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                            return Zson.toJson(response);
                        }
                    }
                }
                return extractMultipleBusInLineData((ServiceDeliveryStructure) sds);
            } else {
                Response response = new Response();
                response.setStatus(Constants.ResponseStatuses.FAILED);
                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
                return response.toString();
            }
        }

    }

    private SingleBusInLine extractSingleBusInLineData(ServiceDeliveryStructure sds) {

        SingleBusInLine singleBus = null;
        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }
            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }

                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            singleBus = new SingleBusInLine(msvs);

                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger()
                        .logEvent("singleBus " + singleBus == null ? " set" : "not  set");
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }

        return singleBus;
    }

    private ArrayList<SingleBusInLine> extractMultipleBusInLineData(ServiceDeliveryStructure sds) {
        ArrayList<SingleBusInLine> list = new ArrayList<>();
        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }
            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }

                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            SingleBusInLine singleBus = new SingleBusInLine(msvs);

                            // since we only need to know the next station of
                            // bus, and we're receiving all future stations,
                            // we only need to add the bus once to the list.
                            // we're checking if we already have it by comparing
                            // existing singlebus item's aimedTepartureTime to
                            // the new one's time.
                            boolean found = false;
                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).getAimedDepartureTime()
                                        .equalsIgnoreCase(singleBus.getAimedDepartureTime())) {
                                    found = true;
                                    break;
                                }
                            }

                            // add only if it's new bus of that line
                            if (!found) {
                                list.add(singleBus);
                            }
                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("fullList size: " + list.size());
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }

        return list;
    }

    private HashMap<String, ArrayList<SingleBusInLine>> extractNewSingleBusInLineData(ServiceDeliveryStructure sds) {
        ArrayList<SingleBusInLine> list = new ArrayList<>();
        HashMap<String, ArrayList<SingleBusInLine>> fullList = new HashMap<>();
        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }
            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }

                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            SingleBusInLine singleBus = new SingleBusInLine(msvs);

                            if (fullList.containsKey(singleBus.getLineRef())) {
                                fullList.get(singleBus.getLineRef()).add(singleBus);
                            } else {
                                fullList.put(singleBus.getLineRef(), new ArrayList<SingleBusInLine>());
                                fullList.get(singleBus.getLineRef()).add(singleBus);
                            }
                            // since we only need to know the single
                            // bus, and we're receiving all future buses,
                            // we only need to add the bus once to the list.
                            // we're checking if we already have it by comparing
                            // existing singlebus item's lineRef and direction
                            // to
                            // the new one's.
                            // boolean found = false;
                            // for (int i = 0; i < list.size(); i++) {
                            // if
                            // (isSameBusLineAndDirecion(list.get(i),singleBus))
                            // {
                            // found = true;
                            // break;
                            // }
                            // }
                            //
                            // // add only if it's new bus of that line
                            // if (!found)
                            // list.add(singleBus);
                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("fullList size: " + list.size());
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }

        return fullList;
    }

    private boolean isSameBusLineAndDirecion(SingleBusInLine singleBusInLine, SingleBusInLine singleBus) {
        if (!singleBusInLine.getLineRef().equalsIgnoreCase(singleBus.getLineRef())) {
            return false;
        }
        if (!singleBusInLine.getDirection().equalsIgnoreCase(singleBus.getDirection())) {
            return false;
        }
        return true;
    }

    private ArrayList<SingleBusInLine> extractNewMultipleBusInLineData(ServiceDeliveryStructure sds) {
        ArrayList<SingleBusInLine> list = new ArrayList<>();
        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }
            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }

                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            SingleBusInLine singleBus = new SingleBusInLine(msvs);

                            // since we only need to get one result for each
                            // trip,
                            // and we're receiving all future lines trips,
                            // we only need to add the bus once to the list.
                            // we're checking if we already have it by comparing
                            // existing singlebus item's lineRef,direction
                            // andaimedDepartureTime to
                            // the new one's.
                            boolean found = false;
                            for (int i = 0; i < list.size(); i++) {
                                if (isSameBus(list.get(i), singleBus)) {
                                    found = true;
                                    break;
                                }
                            }

                            // add only if it's new bus of that line
                            if (!found) {
                                list.add(singleBus);
                            }
                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("fullList size: " + list.size());
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }

        return list;
    }

    private boolean isSameBus(SingleBusInLine singleBusInLine, SingleBusInLine singleBus) {
        if (!singleBusInLine.getLineRef().equalsIgnoreCase(singleBus.getLineRef())) {
            return false;
        }
        if (!singleBusInLine.getAimedDepartureTime().equalsIgnoreCase(singleBus.getAimedDepartureTime())) {
            return false;
        }
        if (!singleBusInLine.getDirection().equalsIgnoreCase(singleBus.getDirection())) {
            return false;
        }
        return true;
    }

    /**
     * @param stationId
     * @param lineId
     * @param startTimeStr
     * @param maxStopVisitsStr
     * @param minStopVisitsPerLineStr
     * @param maxExcpectedArrivalTime
     * @return pure Siri data structured as {@link StopInfo} objects
     */
    public String testGetSiriData(String stationId, String lineId, String startTimeStr, String maxStopVisitsStr,
            String minStopVisitsPerLineStr, String maxExcpectedArrivalTime) {
        if (stationId == null) {
            stationId = "all";
        }
        if (lineId == null) {
            lineId = "all";
        }
        if (startTimeStr == null) {
            startTimeStr = "";
        }
        if (maxStopVisitsStr == null) {
            maxStopVisitsStr = "";
        }
        if (minStopVisitsPerLineStr == null) {
            minStopVisitsPerLineStr = "";
        }
        if (maxExcpectedArrivalTime == null) {
            maxExcpectedArrivalTime = String.valueOf(24 * 60);
        }
        StopMonitoringRequestParams smrp = new StopMonitoringRequestParams(stationId, lineId,
                startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, maxExcpectedArrivalTime);

        Object sds = stopMonitoringQueries.GetStopArrivalTimes(smrp);
        if (sds instanceof ServiceDeliveryStructure) {
            ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
            if (errorCondition != null) {
                if (errorCondition.getDescription() != null) {
                    if (errorCondition.getDescription().getValue() != null) {
                        if (Utils.testSiriResponses) {
                            System.out.println("Got ErrorCondition: " + errorCondition.getDescription().getValue());
                        }

                        Response response = new Response();
                        response.setStatus(Constants.ResponseStatuses.FAILED);
                        response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                        return Zson.toJson(response);
                    }
                }
            }
            return "success: size: " + ((ServiceDeliveryStructure) sds).getStopMonitoringDelivery().size();
        } else {
            return "failed";
        }
    }

    /**
     * @param stationId
     * @param lineId
     * @param startTimeStr
     * @param maxStopVisitsStr
     * @param minStopVisitsPerLineStr
     * @param maxExcpectedArrivalTime
     * @return pure Siri data structured as {@link StopInfo} objects
     */
    public String getSiriData(String stationId, String lineId, String startTimeStr, String maxStopVisitsStr,
            String minStopVisitsPerLineStr, String maxExcpectedArrivalTime) {
        if (stationId == null) {
            stationId = "all";
        }
        if (lineId == null) {
            lineId = "all";
        }
        if (startTimeStr == null) {
            startTimeStr = "";
        }
        if (maxStopVisitsStr == null) {
            maxStopVisitsStr = "";
        }
        if (minStopVisitsPerLineStr == null) {
            minStopVisitsPerLineStr = "";
        }
        if (maxExcpectedArrivalTime == null) {
            maxExcpectedArrivalTime = String.valueOf(24 * 60);
        }
        StopMonitoringRequestParams smrp = new StopMonitoringRequestParams(stationId, lineId,
                startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, maxExcpectedArrivalTime);

        Object sds = stopMonitoringQueries.GetStopArrivalTimes(smrp);
        if (sds instanceof ServiceDeliveryStructure) {
            ErrorCondition errorCondition = ((ServiceDeliveryStructure) sds).getErrorCondition();
            if (errorCondition != null) {
                if (errorCondition.getDescription() != null) {
                    if (errorCondition.getDescription().getValue() != null) {
                        if (Utils.testSiriResponses) {
                            System.out.println("Got ErrorCondition: " + errorCondition.getDescription().getValue());
                        }

                        Response response = new Response();
                        response.setStatus(Constants.ResponseStatuses.FAILED);
                        response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                        return Zson.toJson(response);
                    }
                }
            }
            return extractSiriData((ServiceDeliveryStructure) sds);
        } else {
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.FAILED);
            response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + ((Exception) sds).toString());
            return Zson.toJson(response).toString();
        }
    }

    private String extractSiriData(ServiceDeliveryStructure sds) {

        // represent list of real time data for lines entering to specific
        // station.
        ArrayList<StopInfo> stopInfoList = new ArrayList<>();

        if (sds != null) {
            if (Utils.debug) {
                SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure not Null ");
            }
            if (sds.getStopMonitoringDelivery() != null) {
                if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger()
                            .logEvent("StopMonitoringDelivery size: " + sds.getStopMonitoringDelivery().size());
                }
            }

            for (StopMonitoringDeliveryStructure smds : sds.getStopMonitoringDelivery()) {
                if (smds != null) {
                    if (Utils.debug) {
                        SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure not Null ");
                    }
                    ServiceDeliveryErrorConditionStructure errorCondition = smds.getErrorCondition();
                    if (errorCondition != null) {
                        if (errorCondition.getDescription() != null) {
                            if (errorCondition.getDescription().getValue() != null) {
                                Response response = new Response();
                                response.setStatus(Constants.ResponseStatuses.FAILED);
                                response.setData(Constants.ErrorReasons.SERVER_ERROR + ": " + errorCondition.getDescription().getValue());
                                return Zson.toJson(response);
                            }
                        }
                    }
                    if (smds.getMonitoredStopVisit() != null) {
                        if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger()
                                    .logEvent("MonitoredStopVisit() size: " + smds.getMonitoredStopVisit().size());
                        }
                    }
                    for (MonitoredStopVisitStructure msvs : smds.getMonitoredStopVisit()) {
                        if (msvs != null) {
                            if (Utils.debug) {
                                SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure");
                            }
                            StopInfo stopInfo = new StopInfo(msvs);
                            stopInfoList.add(stopInfo);

                        } else if (Utils.debug) {
                            SiriClientMain.getInstance().getLogger().logEvent("MonitoredStopVisitStructure Null ");
                        }
                    }
                } else if (Utils.debug) {
                    SiriClientMain.getInstance().getLogger().logEvent("StopMonitoringDeliveryStructure Null ");
                }
            }
        } else if (Utils.debug) {
            SiriClientMain.getInstance().getLogger().logEvent("ServiceDeliveryStructure Null ");
        }
        if (stopInfoList.size() > 0) {

        }

        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(stopInfoList, new TypeToken<ArrayList<StopInfo>>() {
        }.getType());

        if (!element.isJsonArray()) {
            // fail appropriately
            SiriClientMain.getInstance().getLogger().logEvent("failed to conver to array");
            ;
        }

        JsonArray jsonArray = element.getAsJsonArray();
        Response response = new Response();
        response.setStatus(Constants.ResponseStatuses.SUCCESS);
        response.setData(jsonArray.toString());
        return Zson.toJson(response);
    }
}
