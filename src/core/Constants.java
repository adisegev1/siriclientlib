package core;

public class Constants {

	public static class ResponseStatuses{
		public final static String SUCCESS = "Success";
		public final static String FAILED = "Failed";
	}
	
	public static class ErrorReasons{
		public final static String SERVER_ERROR = "Server error";
		public final static String MISSING_FIELD_LINEREF = "Missing field lineRef";
		public final static String LIST_PARSE_ERROR = "Error parsing lines list";
	}
	
}
