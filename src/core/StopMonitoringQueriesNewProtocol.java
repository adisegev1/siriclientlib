/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import siriclient.QueryParams;
import sirism.SiriSM;
import uk.org.siri.siri.ServiceDeliveryStructure;
import zlib.Settings;

/**
 *
 * @author adi
 */
public class StopMonitoringQueriesNewProtocol extends StopMonitoringQueries {

    private final Settings settings;
    private final SiriSM siriSm;
    private final SiriClientMain siriClientMain;
    private static final int DEFAULT_PREVIEW_INTERVAL = 1430;
    private int previewInterval = DEFAULT_PREVIEW_INTERVAL;
    private static final String DEFAULT_ALL = "all";

    StopMonitoringQueriesNewProtocol(Settings appSettings) {
        settings = appSettings;
        String tempPreviewInterval = settings.getProperty("default-preview-interval");
        if (tempPreviewInterval != null && !tempPreviewInterval.isEmpty()) {
            try {
                previewInterval = Integer.valueOf(tempPreviewInterval);
            } catch (Exception ex) {
            }
        }

        siriClientMain = SiriClientMain.getInstance();

        siriSm = new SiriSM(settings, null);

        try {
            siriSm.init();
        } catch (Exception ex) {
            siriClientMain.getLogger().logEvent(ex.getMessage());
        }
    }

    @Override
    public Object GetStopArrivalTimes(StopMonitoringRequestParams requestParams) {
        try {
            String stationId = null;
            String lineRef = null;

            if (requestParams.getMonitoringRef() != null) {
                stationId = requestParams.getMonitoringRef().getValue();
            }

            if (requestParams.getLineRef() != null) {
                lineRef = requestParams.getLineRef().getValue();
            }

            // if there's no stationId and lineRef exit
            if ((stationId == null || stationId.isEmpty()) && (lineRef == null || lineRef.isEmpty())) {
                throw new IllegalArgumentException("Request must contain either station ID or lineRef.");
            }

            QueryParams params = new QueryParams(stationId);

            if (lineRef != null && !DEFAULT_ALL.equalsIgnoreCase(stationId) && !DEFAULT_ALL.equalsIgnoreCase(lineRef)) {
                params.setLineRef(requestParams.getLineRef().getValue());
                if (Utils.debug) {
                    siriClientMain.getLogger().logEvent("Lineref set to:" + requestParams.getLineRef().getValue());
                }
            }

            if (requestParams.getMaxStopVisit() != null) {
                params.setMaximumStopVisitsPerLine(requestParams.getMaxStopVisit().intValue());
            }

            if (requestParams.getPreviewInterval() != null && requestParams.getPreviewInterval().getMinutes() > 0) {
                params.setPreviewIntervalMinutes(requestParams.getPreviewInterval().getMinutes() >= 1440 ? previewInterval : requestParams.getPreviewInterval().getMinutes());
            } else {
                params.setPreviewIntervalMinutes(previewInterval);
            }

            if (Utils.debug) {
                String details = "StationID: " + (params.getMonitoringRef() != null ? params.getMonitoringRef() : "null")
                        + ", line: " + (params.getLineRef() != null ? params.getLineRef() : "null")
                        + ", MaxStopVisit: " + (params.getMaximumStopVisitsPerLine() != null ? params.getMaximumStopVisitsPerLine() : "null")
                        + ", PreviewInterval: " + (params.getPreviewIntervalMinutes() != null ? params.getPreviewIntervalMinutes() : "null");
                siriClientMain.getLogger().logEvent(details);
            }
            return siriSm.getServiceDelivery(params);
        } catch (Exception ex) {
            if (Utils.debug) {
                siriClientMain.getLogger().logException(ex);
            }
            return ex;
        }

    }

    @Override
    public Object GetStopArrivalTimesMultipleRequests(List<StopMonitoringRequestParams> requestParamsList) {
        try {
            String stationIds = "";
            String lineRefs = "";

            for (StopMonitoringRequestParams monitoringQueries : requestParamsList) {
                if (monitoringQueries.getMonitoringRef() == null) {
                    continue;
                }

                String stationId = monitoringQueries.getMonitoringRef().getValue();
                if (stationId != null) {
                    stationIds += stationId + ",";
                }
            }

            // if there's station Id's we can't add lineRefs to the query
            if (stationIds.isEmpty() || (!stationIds.isEmpty() && !stationIds.contains(DEFAULT_ALL))) {
                for (StopMonitoringRequestParams monitoringQueries : requestParamsList) {
                    if (monitoringQueries.getLineRef() == null) {
                        continue;
                    }
                    String lineRef = monitoringQueries.getLineRef().getValue();
                    if (lineRef != null && !DEFAULT_ALL.equalsIgnoreCase(lineRef)) {
                        lineRefs += lineRef + ",";
                    }
                }
            }

            if (stationIds.isEmpty() && !lineRefs.contains(DEFAULT_ALL)) {
                stationIds = DEFAULT_ALL;
            } else {
                // remove last ';'
                stationIds = stationIds.substring(0, stationIds.length() - 1);
            }
            if (Utils.debug) {
                siriClientMain.getLogger().logEvent("Stations list: " + stationIds);
                siriClientMain.getLogger().logEvent("LineRefs list: " + lineRefs);
            }

            if (stationIds.contains(DEFAULT_ALL) && lineRefs.contains(DEFAULT_ALL)) {
                throw new IllegalArgumentException("You can't put \"all\" for both station ID and lineRef.");
            }

            StopMonitoringRequestParams requestParams = requestParamsList.get(0);

            QueryParams queryParams = new QueryParams(stationIds);
            if (!lineRefs.isEmpty()) {
                queryParams.setLineRef(lineRefs);
            }

            if (requestParams.getPreviewInterval() != null && requestParams.getPreviewInterval().getMinutes() > 0) {
                queryParams.setPreviewIntervalMinutes(requestParams.getPreviewInterval().getMinutes() >= 1440 ? previewInterval : requestParams.getPreviewInterval().getMinutes());
            } else {
                queryParams.setPreviewIntervalMinutes(previewInterval);
            }

            return siriSm.getServiceDelivery(queryParams);
        } catch (Exception ex) {
            if (Utils.debug) {
                siriClientMain.getLogger().logException(ex);
            }
            return ex;
        }
    }

}
