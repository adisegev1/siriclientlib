/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Adi
 */
public class Utils {
	public static final String settingFileName = "settings.dat";
	static final String PRIMARY_WEB_SERVICE_ADDRESS_PROPERTY_NAME = "web service address";
	static final String ALTERNATE_WEB_SERVICE_ADDRESS_PROPERTY_NAME = "alternate web service address";
	public static String rootFolder = "";
	public static final String logFolderName = "log root folder";
	public static String logRootPath;
	public static boolean debug;
	public static boolean testSiriResponses;
	

	static XMLGregorianCalendar now() {
		return (newCal(System.currentTimeMillis()));
	}

	/**
	 *
	 * @param time
	 * @return
	 */
	public static XMLGregorianCalendar newCal(long time) {
		try {
			DatatypeFactory dtf = DatatypeFactory.newInstance();
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeInMillis(time);
			return (dtf.newXMLGregorianCalendar(cal));
		} catch (DatatypeConfigurationException e) {
			SiriClientMain.getInstance().getLogger().logException(e);
			return (null);
		}
	}

	public static String formatDate(Date date, String fmt) {
		if (date != null) {
			return new SimpleDateFormat(fmt).format(date);
		}
		return "";
	}
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}
}
