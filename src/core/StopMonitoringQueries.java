/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.List;
import uk.org.siri.siri.ServiceDeliveryStructure;

/**
 *
 * @author adi
 */
public abstract class StopMonitoringQueries {
    
    public abstract Object GetStopArrivalTimes(StopMonitoringRequestParams requestParams);

    public abstract Object GetStopArrivalTimesMultipleRequests(List<StopMonitoringRequestParams> requestParamsList);

}
