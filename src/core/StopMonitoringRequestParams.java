/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.math.BigInteger;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import uk.org.siri.siri.LineRefStructure;
import uk.org.siri.siri.MonitoringRefStructure;
import zlib.ZDate;

/**
 *
 * @author adi
 */
class StopMonitoringRequestParams {
    
    private MonitoringRefStructure monitoringRef;
    private LineRefStructure lineRef;
    private XMLGregorianCalendar startTime = null;
    private BigInteger maxStopVisit = null;
    private BigInteger minStopVisitPerLine = null;
    private Duration previewInterval = null;
    private final SiriClientMain siriClientMain;
    public StopMonitoringRequestParams(String monitorRefStr, String lineRefStr, String startTimeStr, String maxStopVisitStr, String minStopVisitPerLineStr, String previewIntervalStr) {
      siriClientMain = SiriClientMain.getInstance();
        (monitoringRef = new MonitoringRefStructure()).setValue(monitorRefStr);
        (lineRef = new LineRefStructure()).setValue(lineRefStr);
        if (maxStopVisitStr != null && !maxStopVisitStr.isEmpty()) {
            maxStopVisit = new BigInteger(maxStopVisitStr);
        }
        if (minStopVisitPerLineStr != null && !minStopVisitPerLineStr.isEmpty()) {
            this.minStopVisitPerLine = new BigInteger(minStopVisitPerLineStr);
        }
        try {
            if (startTimeStr != null && !startTimeStr.isEmpty()) {
                startTime = ZDate.parseTime(startTimeStr, "HH:mm", null).toXMLGregorianCalendar();
            }
            if (previewIntervalStr != null && !previewIntervalStr.isEmpty()) {
                int previewIntervalNum = Integer.parseInt(previewIntervalStr);
                previewInterval = DatatypeFactory.newInstance().newDuration(previewIntervalNum * ZDate.MINUTE);
            }
        } catch (Exception e) {
            siriClientMain.getLogger().logException(e);
        }
        if (Utils.debug) {
            siriClientMain.getLogger().logEvent("StopMonitoringRequestParams set");
        }
    }

    /**
     * @return the monitoringRef
     */
    public MonitoringRefStructure getMonitoringRef() {
        return monitoringRef;
    }

    /**
     * @param monitoringRef the monitoringRef to set
     */
    public void setMonitoringRef(MonitoringRefStructure monitoringRef) {
        this.monitoringRef = monitoringRef;
    }

    /**
     * @return the lineRef
     */
    public LineRefStructure getLineRef() {
        return lineRef;
    }

    /**
     * @param lineRef the lineRef to set
     */
    public void setLineRef(LineRefStructure lineRef) {
        this.lineRef = lineRef;
    }

    /**
     * @return the startTime
     */
    public XMLGregorianCalendar getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(XMLGregorianCalendar startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the maxStopVisit
     */
    public BigInteger getMaxStopVisit() {
        return maxStopVisit;
    }

    /**
     * @param maxStopVisit the maxStopVisit to set
     */
    public void setMaxStopVisit(BigInteger maxStopVisit) {
        this.maxStopVisit = maxStopVisit;
    }

    /**
     * @return the previewInterval
     */
    public Duration getPreviewInterval() {
        return previewInterval;
    }

    /**
     * @param previewInterval the previewInterval to set
     */
    public void setPreviewInterval(Duration previewInterval) {
        this.previewInterval = previewInterval;
    }

    /**
     * @return the minStopVisitPerLine
     */
    public BigInteger getMinStopVisitPerLine() {
        return minStopVisitPerLine;
    }

    @Override
    public String toString() {
        return "StopMonitoringRequestParams{" + "monitoringRef=" + monitoringRef + ", lineRef=" + lineRef + ", startTime=" + startTime + ", maxStopVisit=" + maxStopVisit + ", minStopVisitPerLine=" + minStopVisitPerLine + ", previewInterval=" + previewInterval + '}';
    }
    
    
}
