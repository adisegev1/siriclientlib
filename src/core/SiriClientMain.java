package core;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import core.Constants.ErrorReasons;
import core.Constants.ResponseStatuses;
import static core.Utils.PRIMARY_WEB_SERVICE_ADDRESS_PROPERTY_NAME;
import entities.RealTimeData;
import entities.Response;
import entities.SingleBusInLine;
import entities.StopInfo;
import siriclient.QueryParams;
import zlib.Settings;
import zlib.ZLog;
import zlib.Zson;
import zlib.Ztring;

/**
 *
 * @author Adi
 */
public class SiriClientMain {

    public static final String SM_VERSION = "SM-Version";
    public static final String DEBUG = "debug";
    private static SiriClientMain siriClientMainInstance;
    private StopMonitoringQueries stopMonitoringQueries;
    private final Settings settings;
    private ZLog logger;

    private final static String VERSION = "2.1"; // Ver 2.1 Add support for new server.New server currenly supports only 2.8 version of siri.
                                                 // Add debug and default-preview-interval flags to settings.

    private SiriClientMain() {
        settings = new Settings(Utils.settingFileName);
    }

    public static SiriClientMain getInstance() {
        if (siriClientMainInstance == null) {
            siriClientMainInstance = new SiriClientMain();
        }
        return siriClientMainInstance;
    }

    public boolean init(String rootFolder) {

        Utils.rootFolder = new Ztring(rootFolder).prefixDevice();
        settings.init(Utils.rootFolder);
        logger = new ZLog(Utils.rootFolder, settings);
        try {
            logger.init(Utils.logFolderName);
            logger.logEvent("logger init finished. logging to: " + logger.getRootPath());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        File wsdl = new File(new Ztring(Utils.rootFolder + "/wsdl/siri_wsProducer - Israel.wsdl").prefixDevice());
        if (!wsdl.exists()) {
            logger.logEvent("couldn't find wsdl file in: " + wsdl.getAbsolutePath());
            return false;
        } else {
            logger.logEvent("wsdl exists");
        }
        try {
            boolean debug = settings.getProperty("DEBUG") == null ? false : Boolean.valueOf(settings.getProperty(DEBUG));
            setDebug(debug);
        } catch (Exception ex) {
            logger.logEvent("illegal debug property: " + settings.getProperty(DEBUG));
        }
        String version = settings.getProperty(SM_VERSION) == null ? "2.7" : settings.getProperty(SM_VERSION);
        boolean useNewServer = false;
        try {
            useNewServer = settings.getProperty("use-new-server") == null ? false : Boolean.valueOf(settings.getProperty("use-new-server"));
        } catch (Exception e) {
            logger.logEvent("illegal use-new-server property: " + settings.getProperty("use-new-server") + ". Using old server!");
        }

        if (useNewServer) {
            if ("2.8".equalsIgnoreCase(version)) {
                stopMonitoringQueries = new StopMonitoringQueriesNewProtocol(settings);
            } else {
                logger.logEvent("New server not supporting " + version + " version!");
                return false;
            }
        } else if ("2.7".equalsIgnoreCase(version)) {
            stopMonitoringQueries = new StopMonitoringQueriesOldProtocol(settings);
        } else {
            logger.logEvent("Old server not supporting " + version + " version!");
            return false;
        }

        logger.logEvent("SiriClientMain init finished. SiriClientMain version: " + VERSION);
        logger.logEvent("Server Address: " + (useNewServer ? settings.getProperty("siri sm url") : settings.getProperty(PRIMARY_WEB_SERVICE_ADDRESS_PROPERTY_NAME)));
        logger.logEvent("Using " + version + " protocol");
        return true;
    }
    static boolean test;

    public static void main(String[] args) throws Exception {
        SiriClientMain siriMain = SiriClientMain.getInstance();
        boolean init = siriMain.init(prefixDevice("/ISR/SiriClientNew/"));
        if (!init) {
            System.exit(0);
        }
        String result = "";
        if (args == null || args.length == 0) {
            ArrayList<String> ids = new ArrayList<>();
//	    	   ids.add("37057");
            ids.add("54835");
            result = siriMain.getStationRealData(ids, null, true, true, true);

        } else if ("line".equalsIgnoreCase(args[0])) {
            String station = args[1];
            if ("null".equalsIgnoreCase(station)) {
                station = null;
            }
            String line = args[2];
            if ("null".equalsIgnoreCase(line)) {
                line = null;
            }
            boolean returnMultipleTripsPerLIne = false;
            try {
                returnMultipleTripsPerLIne = Boolean.parseBoolean(args[3]);
            } catch (Exception ex) {

            }
            result = siriMain.getLineBasicRealData(station, line, null, returnMultipleTripsPerLIne);
        } else if ("station".equalsIgnoreCase(args[0])) {
            String station = args[1];
            if ("null".equalsIgnoreCase(station)) {
                station = null;
            }
            boolean returnMultipleTripsPerLIne = false;
            boolean returnBusesPassedStation = false;
            boolean returnBusesCurrentStation = false;
            try {
                returnMultipleTripsPerLIne = Boolean.parseBoolean(args[2]);
                returnBusesPassedStation = Boolean.parseBoolean(args[3]);
                returnBusesCurrentStation = Boolean.parseBoolean(args[4]);
            } catch (Exception ex) {

            }

            result = siriMain.getStationRealData(station, null, returnMultipleTripsPerLIne, returnBusesPassedStation, returnBusesCurrentStation);

        } else if ("test".equalsIgnoreCase(args[0])) {
            test(siriMain, args[1]);
            return;
        } else if ("testOneByOne".equalsIgnoreCase(args[0])) {
            testOneByOne(siriMain, args[1]);
            return;
        }

        try {
            JSONObject item = new JSONObject(result);
            String message = item.getString("status");
            if (!Constants.ResponseStatuses.SUCCESS.equalsIgnoreCase(message)) {
                String data = item.getString("data");
                System.out.println("error: " + data);
            } else {
                try {
                    JSONArray data = new JSONArray(item.getString("data"));
//	        			  System.out.println(data);
                    for (int i = 0; i < data.length(); i++) {
                        System.out.println("item array: " + data.get(i));
                    }

                } catch (JSONException ex) {
                    JSONObject data = new JSONObject(item.getString("data"));
                    String time = data.getString("aimedDepartureTime");
                    System.out.println("aimedDepartureTime: " + time);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            JSONObject item = new JSONObject(result);
            System.out.println("JSONObject item: " + item);
            JSONArray data = new JSONArray(item.getString("data"));
            System.out.println("item: " + data.getJSONObject((0)).getString("aimedDepartureTime"));
        }
        System.out.println(System.currentTimeMillis());
    }

    private static void testOneByOne(SiriClientMain siriMain, String threadsNum) {
        Utils.testSiriResponses = true;
        index = 0;
        long allThreadsStart = System.currentTimeMillis();
        System.out.println("Start Threads: " + new Date());

        int num = Utils.isInteger(threadsNum) ? Integer.parseInt(threadsNum) : 5;
        for (int i = 0; i < num; i++) {
            long start = System.currentTimeMillis();
            String result = siriMain.getStationRealData("37057", null, true, true, true);
            System.out.println("Time: " + (System.currentTimeMillis() - start) + " Thread Id: " + Thread.currentThread().getId() + " result length: " + result.length());
            index++;
            if (index == num) {
                System.out.println("Finish Threads: " + new Date() + ". Threads Total: " + (System.currentTimeMillis() - allThreadsStart));
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
    static int index;

    private static void test(SiriClientMain siriMain, String threadsNum) {
//	       String result = siriMain.getLineNextStation("10466",true);

//        String result=siriMain.getLineBasicRealData(null,"10466",true);
//       String result=siriMain.getSiriData(null,"10466",null,null,null,null);
//       
//       for (int i = 0;i<3;i++){
//    	   System.out.println("Start Loop");
//    	   String siriNextBas=siriMain.getStationRealData("37057",false,true,false);
////	       System.out.println("item: "+ siriNextBas);
//	       System.out.println("Time: "+(System.currentTimeMillis()-start));
//	       start = System.currentTimeMillis();
//	       siriNextBas=siriMain.getStationRealData("37057",false,true,true);
//	       System.out.println("Time: "+(System.currentTimeMillis()-start));
//	          
//       }
        Utils.testSiriResponses = true;
        int num = Utils.isInteger(threadsNum) ? Integer.parseInt(threadsNum) : 5;
        index = 0;
        long allThreadsStart = System.currentTimeMillis();
        System.out.println("Start Threads: " + new Date());
        for (int i = 0; i < num; i++) {
            new Thread(new Runnable() {

                @Override
                public void run() {

                    long start = System.currentTimeMillis();
                    System.out.println("Thread Id: " + Thread.currentThread().getId() + " Start Time: " + new Date());
                    String result = siriMain.getStationRealData("37057", null, false, true, true);
                    System.out.println("Thread Id: " + Thread.currentThread().getId() + " result length: " + result.length() + " End Time: " + new Date() + ". Total: " + (System.currentTimeMillis() - start));
                    index++;
                    if (index == num) {
                        System.out.println("Finish Threads: " + new Date() + ". Threads Total: " + (System.currentTimeMillis() - allThreadsStart));
                    }
                }
            }).start();

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private String testSiriData(String stationId, String lineId) {
        return new JsonStructureResponseBuilder(stopMonitoringQueries).testGetSiriData(stationId, lineId, null, null, null, null);
    }

    public static void setDebug(boolean debugMoede) {
        Utils.debug = debugMoede;
    }

    public static void setTestSiriResponses(boolean debugMoede) {
        Utils.testSiriResponses = debugMoede;
    }

    /**
     * This method returns details about line's next bus station.<br>
     * Each item structured as {@link SingleBusInLine} object.
     *
     * @param lineRef lineId
     * @param returnMultipleBusesPerLIne
     * @return JSONObject structured as Response object.<br>
     * In case of success message field is {@link ResponseStatuses#SUCCESS}<br>
     * If returnMultipleBuses is true, data field is a list of
     * {@link SingleBusInLine} objects structured as {@link JSONArray}.<br>
     * Else returns single {@link SingleBusInLine} objects as
     * {@link JSONObject}.<br>
     * In case of error, message field is one of {@link ResponseStatuses} error
     * messages<br>
     * In that case, data field would be simple String with one of
     * {@link ErrorReasons}.
     */
    private String getLineNextStation(String lineRef, boolean returnMultipleBusesPerLIne) {
        // TODO Auto-generated method stub
        Object result = new JsonStructureResponseBuilder(stopMonitoringQueries).getLineNextStationDetails(lineRef, returnMultipleBusesPerLIne);

        if (result instanceof Response) {
            return Zson.toJson(result);
        } else if (result instanceof SingleBusInLine) {
            Response response = new Response();
            response.setStatus(Constants.ResponseStatuses.SUCCESS);
            response.setData(Zson.toJson(result));
            return Zson.toJson(response);
        }

        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(result, new TypeToken<ArrayList<SingleBusInLine>>() {
        }.getType());

        if (!element.isJsonArray()) {
            // fail appropriately 
            SiriClientMain.getInstance().getLogger().logEvent("failed to convert to array");;
        }

        JsonArray jsonArray = element.getAsJsonArray();
        Response response = new Response();
        response.setStatus(Constants.ResponseStatuses.SUCCESS);
        response.setData(jsonArray.toString());
        return Zson.toJson(response).toString();
    }

    /**
     * @return the logger
     */
    public ZLog getLogger() {
        return logger;
    }

    /**
     * This method returns basic real time data about lines or stops.
     *
     * @param stationId
     * @param lineId
     * @param startTimeStr
     * @param maxStopVisitsStr
     * @param minStopVisitsPerLineStr
     * @param maxExcpectedArrivalTime in minutes
     * @return JSONObject structured as Response object.<br>
     * In case of success message field is {@link ResponseStatuses#SUCCESS}<br>
     * If returnMultipleBuses is true, data field is a list of
     * {@link SingleBusInLine} objects structured as {@link JSONArray}.<br>
     * Else returns single {@link SingleBusInLine} objects as
     * {@link JSONObject}.<br>
     * In case of error, message field is one of {@link ResponseStatuses} error
     * messages<br>
     * In that case, data field would be simple String with one of
     * {@link ErrorReasons}.
     *
     */
    public String getBasicRealData(String stationId, String lineId, String startTimeStr, String maxStopVisitsStr, String minStopVisitsPerLineStr, String maxExcpectedArrivalTime) {
        return new JsonStructureResponseBuilder(stopMonitoringQueries).getBasicRealData(stationId, lineId, startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, maxExcpectedArrivalTime, true, true, false);
    }

    /**
     * This method returns basic real time data about given station.
     *
     * @param stationId
     * @param period (in seconds)
     * @param returnMultipleTripsPerLIne if true returns multiple buses with
     * same lineRef, else, returns only the next bus per lineRef
     * @param returnBusesPassedStation if true returns also buses arrived and
     * passed given station, else, returns only the buses suppose to pass
     * through given station and didn't arrived yet.
     * @return If we've successfully got data, RealTimeData list structured as
     * JSONArray. See {@link RealTimeData}.<br>
     * In case of error, ErrorResponse structured as JSONObject. See
     * {@link Response}
     *
     */
    public String getStationRealData(String stationId, String period, boolean returnMultipleTripsPerLIne, boolean returnBusesPassedStation, boolean returnBusesCurrentStation) {
        return new JsonStructureResponseBuilder(stopMonitoringQueries).getBasicRealData(stationId, null, null, null, null, period, returnMultipleTripsPerLIne, returnBusesPassedStation, returnBusesCurrentStation);
    }

    /**
     * This method returns basic real time data about given station.
     *
     * @param stationId
     * @param period (in seconds)
     * @param returnMultipleTripsPerLIne if true returns multiple buses with
     * same lineRef, else, returns only the next bus per lineRef
     * @param returnBusesPassedStation if true returns also buses arrived and
     * passed given station, else, returns only the buses suppose to pass
     * through given station and didn't arrived yet.
     * @return If we've successfully got data, RealTimeData list structured as
     * JSONArray. See {@link RealTimeData}.<br>
     * In case of error, ErrorResponse structured as JSONObject. See
     * {@link Response}
     *
     */
    public String getStationRealData(ArrayList<String> stationsId, String period, boolean returnMultipleTripsPerLIne, boolean returnBusesPassedStation, boolean returnBusesCurrentStation) {
        return new JsonStructureResponseBuilder(stopMonitoringQueries).getBasicRealData(stationsId, null, null, null, null, period, returnMultipleTripsPerLIne, returnBusesPassedStation, returnBusesCurrentStation);
    }

    /**
     * This method returns basic real time data about given station related to
     * given line.
     *
     * @param stationId
     * @param lineId
     * @param period (in seconds)
     * @param returnMultipleBusesPerLIne if true returns multiple buses with
     * same lineId, else, returns only the next bus per lineId
     *
     * @return JSONObject structured as Response object.<br>
     * In case of success message field is {@link ResponseStatuses#SUCCESS}<br>
     * If returnMultipleBuses is true, data field is a list of
     * {@link SingleBusInLine} objects structured as {@link JSONArray}.<br>
     * Else returns single {@link SingleBusInLine} objects as
     * {@link JSONObject}.<br>
     * In case of error, message field is one of {@link ResponseStatuses} error
     * messages<br>
     * In that case, data field would be simple String with one of
     * {@link ErrorReasons}.
     */
    public String getLineBasicRealData(String stationId, String lineId, String period, boolean returnMultipleBuses) {
        try {
            return new JsonStructureResponseBuilder(stopMonitoringQueries).getBasicRealData(stationId, lineId, null, null, null, period, returnMultipleBuses, false, false);
        } catch (Exception ex) {
            logger.logException(ex);
            return null;
        }

    }
    /**
     * @param stationId
     * @param lineId
     * @param startTimeStr
     * @param maxStopVisitsStr
     * @param minStopVisitsPerLineStr
     * @param maxExcpectedArrivalTime
     * @return pure Siri data structured as {@link StopInfo} objects
     */
    public String getSiriData(String stationId, String lineId, String startTimeStr, String maxStopVisitsStr, String minStopVisitsPerLineStr, String maxExcpectedArrivalTime) {
        return new JsonStructureResponseBuilder(stopMonitoringQueries).getSiriData(stationId, lineId, startTimeStr, maxStopVisitsStr, minStopVisitsPerLineStr, maxExcpectedArrivalTime);
    }
    /**
     * Return the full path of the file that consists of this string prefixed by
     * first d: then c: If none of them has the file return null
     *
     * @return the full path with the first device that has it
     */
    public static String prefixDevice(String value) {
        if (value == null || value.isEmpty() || !value.startsWith("/") && !value.startsWith("\\")) {
            return value;
        }
        for (char c = 'c'; c <= 'z'; c--) {
            String name = c + ":" + value;
            if (new File(name).exists()) {
                return name;
            }
        }
        return null;
    }
}
